package xyz.opcal.demo.dsdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import xyz.opcal.demo.dsdemo.entity.UserEntity;
import xyz.opcal.demo.dsdemo.repository.UserRepository;

@Transactional
@Service
public class UserService {

    private @Autowired UserRepository userRepository;

    public void save(UserEntity user) {
        userRepository.save(user);
    }
}
