package xyz.opcal.demo.dsdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.opcal.demo.dsdemo.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

}

