package xyz.opcal.demo.dsdemo.service;

import java.util.List;

import lombok.AllArgsConstructor;
import xyz.opcal.demo.dsdemo.entity.UserEntity;
import xyz.opcal.tools.client.RandomuserClient;
import xyz.opcal.tools.request.RandomuserRequest;
import xyz.opcal.tools.request.param.Nationalities;

@AllArgsConstructor
public class UserUtils {

	private RandomuserClient randomuserClient;

	public List<UserEntity> generate(int batch) {
		var response = randomuserClient.random(RandomuserRequest.builder().results(batch)
				.nationalities(new Nationalities[] { Nationalities.AU, Nationalities.GB, Nationalities.CA, Nationalities.US, Nationalities.NZ }).build());
		return response.getResults().stream().map(this::toUser).toList();
	}

	UserEntity toUser(xyz.opcal.tools.response.result.User result) {
		UserEntity user = new UserEntity();
		user.setFirstName(result.getName().getFirst());
		user.setLastName(result.getName().getLast());
		user.setGender(result.getGender());
		user.setAge(result.getDob().getAge());
		return user;
	}
}
