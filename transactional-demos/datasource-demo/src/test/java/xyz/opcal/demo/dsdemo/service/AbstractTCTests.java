package xyz.opcal.demo.dsdemo.service;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;

import xyz.opcal.demo.configuration.MysqlTcConfiguration;
import xyz.opcal.tools.client.RandomuserClient;

@Import(MysqlTcConfiguration.class)
abstract class AbstractTCTests {

	@Autowired
	JdbcTemplate jdbcTemplate;
	@Autowired
	UserService userService;
	
	RandomuserClient randomuserClient = new RandomuserClient(System.getenv().getOrDefault("RANDOMUSER_API_URL", RandomuserClient.DEFAULT_API_URL));

	UserUtils userUtils;

	@BeforeAll
	void init() {
		userUtils = new UserUtils(randomuserClient);
	}

	@AfterAll
	void after() {
		final List<Map<String, Object>> users = jdbcTemplate.queryForList("SELECT * FROM user");
		System.out.println("user info: \n" + users);
	}

}
