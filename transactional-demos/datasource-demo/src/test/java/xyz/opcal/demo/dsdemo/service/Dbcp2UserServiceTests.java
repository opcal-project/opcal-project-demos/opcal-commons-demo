package xyz.opcal.demo.dsdemo.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.junit.jupiter.Testcontainers;

import xyz.opcal.demo.dsdemo.entity.UserEntity;

@Testcontainers
@SpringBootTest
@ActiveProfiles("dbcp2")
@TestInstance(Lifecycle.PER_CLASS)
class Dbcp2UserServiceTests extends AbstractTCTests {

	@Test
	void save() {
		UserEntity user = userUtils.generate(1).get(0);
		assertDoesNotThrow(() -> userService.save(user));
	}
}
