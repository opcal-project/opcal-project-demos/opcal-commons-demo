package xyz.opcal.demo.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import org.testcontainers.junit.jupiter.Testcontainers;

import xyz.opcal.demo.configuration.MysqlTcConfiguration;

@Testcontainers
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(Lifecycle.PER_CLASS)
@Import(MysqlTcConfiguration.class)
class UserServiceTests {

	private @Autowired UserService userService;
	private @Autowired JdbcTemplate jdbcTemplate;

	@AfterAll
	void after() {
		final List<Map<String, Object>> users = jdbcTemplate.queryForList("SELECT * FROM user");
		System.out.println("user info: \n" + users);
	}

	@Test
	@Order(0)
	void testSaveInLombda() {
		assertDoesNotThrow(() -> userService.saveInLombda(userService.generate(10)));
	}

	@Test
	@Order(1)
	void testSaveErrorAfter() {
		final long beforeTotal = userService.countAll();
		assertThrows(RuntimeException.class, () -> userService.saveErrorAfter(userService.generate(10)));
		assertEquals(beforeTotal, userService.countAll());
	}

	@Test
	@Order(2)
	void testSaveErrorInforeach() {
		final long beforeTotal = userService.countAll();
		assertThrows(RuntimeException.class, () -> userService.saveErrorInforeach(userService.generate(10)));
		assertEquals(beforeTotal, userService.countAll());
	}

}
