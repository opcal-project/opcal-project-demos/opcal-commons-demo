package xyz.opcal.demo.service;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import xyz.opcal.demo.entity.User;
import xyz.opcal.demo.repository.UserRepository;
import xyz.opcal.tools.client.RandomuserClient;
import xyz.opcal.tools.request.RandomuserRequest;
import xyz.opcal.tools.request.param.Nationalities;

@Service
public class UserService {

	private @Autowired UserRepository userRepository;
	RandomuserClient randomuserClient = new RandomuserClient(System.getenv().getOrDefault("RANDOMUSER_API_URL", RandomuserClient.DEFAULT_API_URL));

	public List<User> generate(int batch) {
		var response = randomuserClient.random(RandomuserRequest.builder().results(batch)
				.nationalities(new Nationalities[] { Nationalities.AU, Nationalities.GB, Nationalities.CA, Nationalities.US, Nationalities.NZ }).build());
		return response.getResults().stream().map(this::toUser).toList();
	}

	User toUser(xyz.opcal.tools.response.result.User result) {
		User user = new User();
		user.setFirstName(result.getName().getFirst());
		user.setLastName(result.getName().getLast());
		user.setGender(result.getGender());
		user.setAge(result.getDob().getAge());
		return user;
	}

	public long countAll() {
		return userRepository.count();
	}

	@Transactional
	public void saveInLombda(List<User> users) {
		users.forEach(userRepository::save);
	}

	@Transactional
	public void saveErrorAfter(List<User> users) {
		users.forEach(userRepository::save);
		throw new RuntimeException("some error in code");
	}

	@Transactional
	public void saveErrorInforeach(List<User> users) {
		Random random = new Random();
		users.forEach(user -> {
			userRepository.save(user);
			if (random.nextInt(users.size()) % 3 == 0) {
				throw new RuntimeException("some error in code");
			}
		});
	}

}
