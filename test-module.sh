#!/bin/bash

./mvnw -U clean install -DskipTests

mkdir -p /tmp/artifact

find ./**/**/target/ -type f -iname '**.jar' | xargs -I {} cp {} /tmp/artifact

# ./mvnw -Dmaven.surefire.debug="-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5000" test -pl "$@"
./mvnw test -pl "$@"

rm -rf  ~/.m2/repository/xyz/opcal/demo/
rm -rf  /tmp/artifact
