package xyz.opcal.demo.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.util.CollectionUtils;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.lifecycle.Startables;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import xyz.opcal.demo.entity.User;

@SpringBootTest
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
class UserServiceTests {

	static final int DATA_SIZE = 10;

	static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:16");

	private @Autowired UserService userService;
	private @Autowired JdbcTemplate jdbcTemplate;
	private @Autowired ObjectMapper objectMapper;

	@DynamicPropertySource
	static void redisProperties(DynamicPropertyRegistry registry) {
		Startables.deepStart(postgreSQLContainer).join();

		registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
		registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
		registry.add("spring.datasource.password", postgreSQLContainer::getPassword);

	}

	@Test
	@Order(0)
	void save() {
		assertDoesNotThrow(() -> userService.generate(DATA_SIZE).forEach(userService::save));
	}

	@Test
	@Order(1)
	void getAll() throws JsonProcessingException {
		final List<User> all = userService.getAll();
		assertEquals(DATA_SIZE, all.size());
		System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(all));
	}

	@Test
	@Order(1)
	void checkHistory() throws JsonProcessingException {
		final List<Map<String, Object>> results = jdbcTemplate.queryForList("SELECT * FROM flyway_schema_history");
		assertFalse(CollectionUtils.isEmpty(results));
		System.out.println("history results: \n" + objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(results));
	}

}
