#!/bin/bash
./mvnw -U clean 
./mvnw clean install -DskipTests -pl demo-commons/randomuser-api,demo-commons/tc-common

./mvnw spring-boot:run -pl "$@"

rm -rf  ~/.m2/repository/xyz/opcal/demo/
