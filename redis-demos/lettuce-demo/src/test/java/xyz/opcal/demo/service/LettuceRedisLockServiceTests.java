package xyz.opcal.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
class LettuceRedisLockServiceTests {

	@Autowired
	RedisLockService redisLockService;

	ThreadPoolTaskScheduler taskScheduler;

	@BeforeAll
	void init() {
		taskScheduler = new ThreadPoolTaskScheduler();
		taskScheduler.setPoolSize(20);
		taskScheduler.setThreadNamePrefix("test-");
		taskScheduler.initialize();
	}

	@AfterAll
	void stop() {
		taskScheduler.shutdown();
	}

	@Test
	void concurrent() {
		final String key = UUID.randomUUID().toString();

		for (int i = 0; i < 1000; i++) {
			taskScheduler.submit(() -> assertEquals(key, redisLockService.get(key)));
		}
		while (taskScheduler.getScheduledThreadPoolExecutor().getQueue().size() > 0) {
			// do nothing
		}
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		}
	}

}
