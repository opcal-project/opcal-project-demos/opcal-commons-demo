package xyz.opcal.demo.service;

import java.util.UUID;
import java.util.concurrent.locks.Lock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.redis.util.RedisLockRegistry;
import org.springframework.stereotype.Service;

@Service
public class RedisLockService {

	private @Autowired RedisLockRegistry redisLockRegistry;

	public String get(String key) {
		final Lock lock = redisLockRegistry.obtain(key);
		try {
			lock.lock();
			return UUID.fromString(key).toString();
		} finally {
			lock.unlock();
		}
	}

}
