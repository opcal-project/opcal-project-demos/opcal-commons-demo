package xyz.opcal.demo.service;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RedissonService {

	private @Autowired RedissonClient redisson;

	public String get(String key) {
		final RLock fairLock = redisson.getFairLock("redisson-lock" + key);
		try {
			fairLock.lock(30, TimeUnit.SECONDS);
			return UUID.fromString(key).toString();
		} finally {
			fairLock.unlock();
		}
	}

}
