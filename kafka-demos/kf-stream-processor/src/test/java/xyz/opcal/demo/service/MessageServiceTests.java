package xyz.opcal.demo.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.time.Duration;
import java.util.Random;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.demo.kafka.KafkaContainerCluster;

@Slf4j
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MessageServiceTests {

	static KafkaContainerCluster kafkaContainerCluster = new KafkaContainerCluster(3, 3);

	@DynamicPropertySource
	static void dynamicProperties(DynamicPropertyRegistry registry) {
		kafkaContainerCluster.start();
		registry.add("spring.kafka.bootstrap-servers", kafkaContainerCluster::getBootstrapServers);
	}

	@Autowired
	MessageService messageService;

	@Autowired
	UserService userService;

	Random random = new Random();

	@Test
	@Order(0)
	void sendMessage() {
		var batch = random.nextInt(10, 50);
		assertDoesNotThrow(() -> messageService.create(batch));
		log.info("$$$$$ created message size {}", batch);
	}

	@Test
	@Order(1)
	void consume() {
		Awaitility.await().atMost(Duration.ofMinutes(5)).until(() -> userService.consumeCount() > 0);
	}

}
