package xyz.opcal.demo.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.graphql.tester.AutoConfigureGraphQlTester;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.graphql.test.tester.GraphQlTester;

import xyz.opcal.demo.dto.PageDTO;
import xyz.opcal.demo.entity.User;
import xyz.opcal.demo.repository.UserRepository;

@SpringBootTest
@AutoConfigureGraphQlTester
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserControllerTests {

	private @Autowired UserRepository userRepository;

	private @Autowired GraphQlTester graphQlTester;

	User user;

	@BeforeAll
	void init() {
		user = userRepository.findFirstByOrderByIdDesc().get();
		assertNotNull(user);
	}

	@Test
	@Order(0)
	void userById() {
		graphQlTester.documentName("userById").variable("id", user.getId()).execute().path("userById.firstName").entity(String.class)
				.isEqualTo(user.getFirstName());
	}

	@Test
	@Order(1)
	void fetchUsers() {
		var pageSize = 50;
		graphQlTester.documentName("fetchUsers").variable("pageNum", 0).variable("pageSize", pageSize).execute().path("fetchUsers").entity(PageDTO.class)
				.matches(p -> p != null && p.pageSize() == pageSize);
	}

}
