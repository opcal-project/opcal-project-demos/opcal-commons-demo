package xyz.opcal.demo.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.lifecycle.Startables;
import org.testcontainers.utility.DockerImageName;

import xyz.opcal.demo.entity.User;

@ActiveProfiles("tc")
@SpringBootTest
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
class UserServiceTests {

	static final int DATA_SIZE = 10;

	private static GenericContainer<?> redis = new GenericContainer<>(DockerImageName.parse("redis:7")).withExposedPorts(6379);
	static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:15");

	private @Autowired UserService userService;

	@DynamicPropertySource
	static void redisProperties(DynamicPropertyRegistry registry) {
		Startables.deepStart(redis, postgreSQLContainer).join();

		System.setProperty("tc.redis.host", redis.getHost());
		System.setProperty("tc.redis.port", String.valueOf(redis.getFirstMappedPort()));

		registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
		registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
		registry.add("spring.datasource.password", postgreSQLContainer::getPassword);

	}

	@Test
	@Order(0)
	void save() {
		assertDoesNotThrow(() -> userService.generate(DATA_SIZE).forEach(userService::save));
	}

	@Test
	@Order(1)
	void getAll() {
		final List<User> all = userService.getAll();
		System.out.println(all);
		assertEquals(DATA_SIZE, all.size());
	}

	@Test
	@Order(2)
	void getCacheAll() {
		final List<User> all = userService.getCacheAll();
		System.out.println(all);
		assertEquals(DATA_SIZE, all.size());
	}
}
