package xyz.opcal.demo.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.lifecycle.Startables;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import xyz.opcal.demo.entity.User;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserServiceR2dbcTests {

	static final int DATA_SIZE = 10;

	static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:15");

	private @Autowired UserService userService;

	@DynamicPropertySource
	static void redisProperties(DynamicPropertyRegistry registry) {
		Startables.deepStart(postgreSQLContainer).join();

		registry.add("spring.r2dbc.url", () -> "r2dbc:postgresql://" + postgreSQLContainer.getHost() + ":" + postgreSQLContainer.getFirstMappedPort() + "/"
				+ postgreSQLContainer.getDatabaseName());
		registry.add("spring.r2dbc.username", postgreSQLContainer::getUsername);
		registry.add("spring.r2dbc.password", postgreSQLContainer::getPassword);

	}

	@Test
	@Order(0)
	void save() {
		final List<User> generateUsers = userService.generate(DATA_SIZE);
		for (User user : generateUsers) {
			final Mono<User> verifierMono = userService.save(user).doOnSuccess(System.out::println);
			StepVerifier.create(verifierMono).assertNext(saved -> assertNotNull(saved.id())).verifyComplete();
		}
	}

	@Test
	@Order(1)
	void getAll() {
		final Flux<User> all = userService.getAll();
		final Mono<Long> countMono = all.doOnNext(System.out::println).publish().autoConnect().count().doOnSuccess(System.out::println);
		StepVerifier.create(countMono).assertNext(total -> assertEquals(DATA_SIZE, total)).verifyComplete();
	}

}
