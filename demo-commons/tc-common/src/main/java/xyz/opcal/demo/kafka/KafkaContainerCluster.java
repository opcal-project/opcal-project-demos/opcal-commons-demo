package xyz.opcal.demo.kafka;

import java.time.Duration;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.rnorth.ducttape.unreliables.Unreliables;
import org.testcontainers.containers.Container;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.lifecycle.Startable;
import org.testcontainers.utility.DockerImageName;

import lombok.Getter;
import lombok.SneakyThrows;

public class KafkaContainerCluster implements Startable {

	public static final String IMAGE_TAG = "latest";

	private final int brokersNum;

	private final Network network;

	private final GenericContainer<?> zookeeper;

	@Getter
	private final Collection<KafkaContainer> brokers;

	public KafkaContainerCluster(int brokersNum, int internalTopicsRf) {
		this(IMAGE_TAG, brokersNum, internalTopicsRf);
	}

	@SuppressWarnings("resource")
	public KafkaContainerCluster(String confluentPlatformVersion, int brokersNum, int internalTopicsRf) {
		this.brokersNum = brokersNum;
		this.network = Network.builder().driver("bridge").build();

		this.zookeeper = new GenericContainer<>(DockerImageName.parse("confluentinc/cp-zookeeper").withTag(confluentPlatformVersion)).withNetwork(network)
				.withNetworkAliases("zookeeper").withEnv("ZOOKEEPER_CLIENT_PORT", String.valueOf(KafkaContainer.ZOOKEEPER_PORT));

		this.brokers = IntStream.range(0, this.brokersNum)
				.mapToObj(brokerNum -> new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka").withTag(confluentPlatformVersion))
						.withNetwork(this.network).withNetworkAliases("broker-" + brokerNum).dependsOn(this.zookeeper)
						.withExternalZookeeper("zookeeper:" + KafkaContainer.ZOOKEEPER_PORT).withEnv("KAFKA_BROKER_ID", brokerNum + "") //
						.withEnv("KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR", internalTopicsRf + "")
						.withEnv("KAFKA_OFFSETS_TOPIC_NUM_PARTITIONS", internalTopicsRf + "")
						.withEnv("KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR", internalTopicsRf + "")
						.withEnv("KAFKA_TRANSACTION_STATE_LOG_MIN_ISR", internalTopicsRf + "").withEnv("KAFKA_LOG_RETENTION_HOURS", "-1")
						.withStartupTimeout(Duration.ofMinutes(1)))
				.toList();

	}

	public String getBootstrapServers() {
		return brokers.stream().map(KafkaContainer::getBootstrapServers).collect(Collectors.joining(","));
	}

	private Stream<GenericContainer<?>> allContainers() {
		return Stream.concat(this.brokers.stream(), Stream.of(this.zookeeper));
	}

	@Override
	@SneakyThrows
	public void start() {
		brokers.forEach(GenericContainer::start);

		Unreliables.retryUntilTrue(30, TimeUnit.SECONDS, () -> {
			Container.ExecResult result = this.zookeeper.execInContainer("sh", "-c",
					"zookeeper-shell zookeeper:" + KafkaContainer.ZOOKEEPER_PORT + " ls /brokers/ids | tail -n 1");
			String brokersOut = result.getStdout();

			return brokersOut != null && brokersOut.split(",").length == this.brokersNum;
		});
	}

	@Override
	public void stop() {
		allContainers().parallel().forEach(GenericContainer::stop);
		this.network.close();
	}

}
