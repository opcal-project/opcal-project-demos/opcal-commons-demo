package xyz.opcal.demo.configuration;

import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.context.annotation.Bean;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.shaded.com.google.common.collect.ImmutableSet;
import org.testcontainers.utility.DockerImageName;

import lombok.SneakyThrows;
import xyz.opcal.demo.TcConstants;

public class RabbitMQTcConfiguration {

	@Bean
	@ServiceConnection(name = "rabbitmq")
	@SneakyThrows
	RabbitMQContainer rabbitMQ() {
		var rabbitMQContainer = new RabbitMQContainer(DockerImageName.parse("rabbitmq:management"));
		rabbitMQContainer.start();
		rabbitMQContainer.execInContainer("rabbitmqadmin", "declare", "vhost", "name=" + TcConstants.TC_RABBITMQ_VHOST);
		rabbitMQContainer.execInContainer("rabbitmqadmin", "declare", "user", "name=" + TcConstants.TC_RABBITMQ_USER,
				"password=" + TcConstants.TC_RABBITMQ_PASSWORD, "tags=" + String.join(",", ImmutableSet.of("administrator")));
		rabbitMQContainer.execInContainer("rabbitmqadmin", "declare", "permission", "vhost=" + TcConstants.TC_RABBITMQ_VHOST,
				"user=" + TcConstants.TC_RABBITMQ_USER, "configure=" + ".*", "write=" + ".*", "read=" + ".*");
		return rabbitMQContainer;
	}
}
