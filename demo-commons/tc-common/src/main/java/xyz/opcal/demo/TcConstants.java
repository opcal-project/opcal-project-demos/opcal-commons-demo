package xyz.opcal.demo;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TcConstants {

	/**
	 * Docker building tag in testcontainers.
	 */
	public static final String TAG = String.valueOf(System.currentTimeMillis());

	/**
	 * Project root path in ci pipeline.
	 */
	public static final String ROOT_PATH = System.getenv("PROJECT_DIR");

	public static final String TC_DB_JDBC_URL = "TC_DB_JDBC_URL";
	public static final String TC_DB_NAME = "TC_DB_NAME";
	public static final String TC_DB_PASSWORD = "TC_DB_PASSWORD";

	public static final String TC_RABBITMQ_USER = "tc";
	public static final String TC_RABBITMQ_PASSWORD = "tc";
	public static final String TC_RABBITMQ_VHOST = "/tc";

}
