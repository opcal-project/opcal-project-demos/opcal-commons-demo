package xyz.opcal.demo.configuration;

import java.nio.file.Paths;

import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.context.annotation.Bean;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.utility.DockerImageName;
import org.testcontainers.utility.MountableFile;

import xyz.opcal.demo.TcConstants;

public class MysqlTcConfiguration {

	@Bean
	@ServiceConnection(name = "mysql")
	MySQLContainer<?> mysql() {
		return defaultMysql();
	}

	@SuppressWarnings("resource")
	public static MySQLContainer<?> defaultMysql() {
		// @formatter:off
		return new MySQLContainer<>(DockerImageName.parse("mysql:8.0"))
				.withCopyFileToContainer(MountableFile.forHostPath(Paths.get(TcConstants.ROOT_PATH, "sql/")), "/docker-entrypoint-initdb.d/")
				.withCommand("--character-set-server=utf8mb4", "--collation-server=utf8mb4_unicode_ci")
				.withUrlParam("characterEncoding", "utf-8")
				.withUrlParam("autoReconnect", "true")
				.withUrlParam("useSSL", "false")
				.withUrlParam("allowPublicKeyRetrieval", "true")
				.withExposedPorts(3306);
		// @formatter:on
	}
}
