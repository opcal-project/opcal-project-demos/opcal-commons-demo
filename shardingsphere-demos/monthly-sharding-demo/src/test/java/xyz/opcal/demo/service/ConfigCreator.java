package xyz.opcal.demo.service;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.ResourceUtils;

public class ConfigCreator {

	public static String create(Map<String, Object> variables) {
		try {
			ExpressionParser parser = new SpelExpressionParser();
			Expression expression = parser.parseExpression(IOUtils.toString(ResourceUtils.getURL("classpath:monthly-template.yaml"), StandardCharsets.UTF_8),
					new TemplateParserContext());
			StandardEvaluationContext context = new StandardEvaluationContext();
			context.setVariables(variables);

			var targetFile = FileUtils.getFile(FileUtils.getTempDirectory(), "monthly-" + System.currentTimeMillis() + ".yml");

			FileUtils.write(targetFile, expression.getValue(context, String.class), StandardCharsets.UTF_8);
			return targetFile.getAbsolutePath();
		} catch (Exception e) {
			throw new IllegalStateException("create table sql error", e);
		}
	}

}
