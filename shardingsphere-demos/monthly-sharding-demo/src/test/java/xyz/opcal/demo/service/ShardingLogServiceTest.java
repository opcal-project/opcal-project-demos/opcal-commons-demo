package xyz.opcal.demo.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.util.CollectionUtils;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.images.builder.ImageFromDockerfile;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.lifecycle.Startables;

import xyz.opcal.demo.TcConstants;
import xyz.opcal.demo.configuration.MysqlTcConfiguration;
import xyz.opcal.demo.entity.ShardingLogEntity;

@Testcontainers
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ShardingLogServiceTest {

	private static final String[] LEVELS = new String[] { "TRACE", "DEBUG", "INFO", "WARN", "ERROR" };
	private static final DateTimeFormatter YEAR_MONTH_FORMATTER = DateTimeFormatter.ofPattern("yyyyMM");
	private static final Random random = new Random();

	static Map<String, String> tcProfile = new HashMap<>();

	static MySQLContainer<?> mysql = MysqlTcConfiguration.defaultMysql();

	// @formatter:off
	static GenericContainer<?> checker = new GenericContainer<>(new ImageFromDockerfile("monthly-table-checker:" + TcConstants.TAG, true)
			.withFileFromFile("/tmp/artifact/monthly-table-checker.jar", new File("/tmp/artifact/monthly-table-checker.jar"))
			.withFileFromFile("Dockerfile", Paths.get(TcConstants.ROOT_PATH, "shardingsphere-demos/monthly-table-checker/Dockerfile").toFile())
	).withExposedPorts(8080);
	// @formatter:on

	static String createUrl;
	static String checkUrl;

	@DynamicPropertySource
	static void dynamicProperties(DynamicPropertyRegistry registry) {
		Startables.deepStart(mysql).join();
		tcProfile.put("SPRING_PROFILES_ACTIVE", "tc");
		tcProfile.put(TcConstants.TC_DB_JDBC_URL, mysql.getJdbcUrl());
		tcProfile.put(TcConstants.TC_DB_NAME, mysql.getUsername());
		tcProfile.put(TcConstants.TC_DB_PASSWORD, mysql.getPassword());
		registry.add("spring.datasource.url", () -> "jdbc:shardingsphere:absolutepath:" + ConfigCreator.create(new HashMap<>(tcProfile)));

		checker.withEnv(tcProfile);
		Startables.deepStart(checker).join();

		createUrl = "http://" + checker.getHost() + ":" + checker.getFirstMappedPort() + "/monthly/tables/create";
		checkUrl = "http://" + checker.getHost() + ":" + checker.getFirstMappedPort() + "/monthly/tables/";
	}

	static ShardingLogEntity randomNow() {
		String log = "mock log " + System.currentTimeMillis() + " " + RandomStringUtils.randomAlphabetic(random.nextInt(10, 30));
		// avoid mysql rounding millisecond
		return create(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)), log, LEVELS[random.nextInt(LEVELS.length)]);
	}

	static ShardingLogEntity random() {
		Date createDate = Date.from(LocalDateTime.of(LocalDate.of(Year.now().getValue(), random.nextInt(1, 13), random.nextInt(1, 29)), LocalTime.now())
				.atZone(ZoneId.systemDefault()).toInstant().truncatedTo(ChronoUnit.SECONDS)); // avoid mysql rounding millisecond

		String log = "mock log " + System.currentTimeMillis() + " " + RandomStringUtils.randomAlphabetic(random.nextInt(10, 30));
		return create(createDate, log, LEVELS[random.nextInt(LEVELS.length)]);
	}

	static ShardingLogEntity create(Date createDate, String log, String level) {
		ShardingLogEntity entity = new ShardingLogEntity();
		entity.setCreateDate(createDate);
		entity.setLog(log);
		entity.setLevel(level);
		return entity;
	}

	@Autowired
	ShardingLogService shardingLogService;

	@Autowired
	TestRestTemplate restTemplate;

	Date queryDate;
	long queryId;

	@BeforeAll
	void init() {
		// init tables
		List<String> months = new ArrayList<>();
		for (int i = 1; i <= 12; i++) {
			months.add(YearMonth.of(Year.now().getValue(), i).format(YEAR_MONTH_FORMATTER));
		}
		var response = restTemplate.postForEntity(createUrl, months, String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());

		System.out.println(Arrays.toString(restTemplate.getForEntity(checkUrl, String[].class).getBody()));

		// init data
		for (var i = 0; i < 500; i++) {
			shardingLogService.save(random());
			shardingLogService.save(randomNow());
		}

		var result = shardingLogService.save(random());
		queryDate = result.getCreateDate();
		queryId = result.getId();
	}

	@AfterAll
	void after() {
		var month = YearMonth.from(queryDate.toInstant().atZone(ZoneId.systemDefault()));
		var start = Date.from(LocalDateTime.of(month.atDay(1), LocalTime.of(23, 59, 59)).atZone(ZoneId.systemDefault()).toInstant());
		var end = Date.from(LocalDateTime.of(month.atEndOfMonth(), LocalTime.of(23, 59, 59)).atZone(ZoneId.systemDefault()).toInstant());
		var shardingLogs = shardingLogService.getShardingLogInRange(start, end);
		shardingLogs.forEach(System.out::println);
	}

	@Test
	@Order(0)
	void save() {
		assertDoesNotThrow(() -> shardingLogService.save(randomNow()));
		assertDoesNotThrow(() -> shardingLogService.save(random()));
		assertDoesNotThrow(() -> shardingLogService.save(random()));
		assertDoesNotThrow(() -> shardingLogService.save(random()));
	}

	@Test
	@Order(1)
	void getShardingLogDate() {
		var shardingLogs = shardingLogService.getShardingLog(queryDate);
		assertFalse(CollectionUtils.isEmpty(shardingLogs));
		shardingLogs.forEach(System.out::println);
	}

	@Test
	@Order(2)
	void getShardingLogBetween() {
		var end = Date.from(LocalDateTime.of(YearMonth.from(queryDate.toInstant().atZone(ZoneId.systemDefault())).atEndOfMonth(), LocalTime.of(23, 59, 59))
				.atZone(ZoneId.systemDefault()).toInstant());

		var shardingLogs = shardingLogService.getShardingLogInRange(queryDate, end);
		assertFalse(CollectionUtils.isEmpty(shardingLogs));
		shardingLogs.forEach(System.out::println);
	}

	@Test
	@Order(3)
	void getShardingLogId() {
		ShardingLogEntity shardingLog = shardingLogService.getShardingLog(queryId, queryDate);
		assertNotNull(shardingLog);
		System.out.println(shardingLog);
	}

}
