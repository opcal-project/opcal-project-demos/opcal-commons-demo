package xyz.opcal.demo.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.testcontainers.junit.jupiter.Testcontainers;

import xyz.opcal.demo.AbstractTcTests;

@Testcontainers
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class MonthlyTableControllerTests extends AbstractTcTests {

	private static final int MONTHS_SIZE = 12;

	@Autowired
	TestRestTemplate restTemplate;

	@Test
	void test() {
		final YearMonth now = YearMonth.now();
		List<String> months = new ArrayList<>();
		months.add(now.format(YEAR_MONTH_FORMATTER));
		for (long i = 1; i <= MONTHS_SIZE; i++) {
			months.add(now.plusMonths(i).format(YEAR_MONTH_FORMATTER));
			months.add(now.plusMonths(i * -1).format(YEAR_MONTH_FORMATTER));
		}
		var response = restTemplate.postForEntity("/monthly/tables/create", months, String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	void allTables() {
		var response = restTemplate.getForEntity("/monthly/tables/", String[].class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		System.out.println("###");
		System.out.println(Arrays.toString(response.getBody()));
	}
}
