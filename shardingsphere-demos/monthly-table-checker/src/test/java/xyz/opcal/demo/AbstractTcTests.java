package xyz.opcal.demo;

import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;

import xyz.opcal.demo.configuration.MysqlTcConfiguration;

@Import(MysqlTcConfiguration.class)
@TestInstance(Lifecycle.PER_CLASS)
public abstract class AbstractTcTests {

	public static final DateTimeFormatter YEAR_MONTH_FORMATTER = DateTimeFormatter.ofPattern("yyyyMM");

	@Autowired
	protected JdbcTemplate jdbcTemplate;

	@AfterAll
	void after() {
		System.out.println(jdbcTemplate.queryForList("SELECT table_schema, table_name FROM information_schema.TABLES WHERE table_name LIKE 'sharding_log_%'"));
	}

}
