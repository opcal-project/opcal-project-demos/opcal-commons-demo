package xyz.opcal.demo.checker;

import java.time.YearMonth;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.junit.jupiter.Testcontainers;

import xyz.opcal.demo.AbstractTcTests;

@Testcontainers
@SpringBootTest
class MonthlyTableCheckerTests extends AbstractTcTests {

	@Autowired
	MonthlyTableChecker monthlyTableCheck;

	@Test
	void test() {
		monthlyTableCheck.checkAndCreateTable(YearMonth.now().format(YEAR_MONTH_FORMATTER));
	}

}
