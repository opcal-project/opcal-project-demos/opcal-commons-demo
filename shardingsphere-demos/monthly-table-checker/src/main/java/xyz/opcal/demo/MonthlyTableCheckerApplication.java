package xyz.opcal.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonthlyTableCheckerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonthlyTableCheckerApplication.class, args);
	}

}
