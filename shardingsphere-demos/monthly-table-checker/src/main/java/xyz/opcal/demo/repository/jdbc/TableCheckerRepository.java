package xyz.opcal.demo.repository.jdbc;

import java.sql.Connection;
import java.sql.Types;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class TableCheckerRepository {

	private static final String TABLE_CHECK_SQL = """
			SELECT COUNT(*) FROM information_schema.TABLES WHERE table_schema = ? AND table_name = ?
			""";

	private static final String ALL_TABLES_SQL = """
			SELECT table_name FROM information_schema.TABLES WHERE table_schema = ? AND table_name like ?
			""";

	private JdbcTemplate jdbcTemplate;
	private String jdbcUrl;

	public TableCheckerRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
		this.jdbcUrl = getJdbcUrl();
	}

	public boolean existeTable(String tableName) {
		return Optional.ofNullable(
				jdbcTemplate.queryForObject(TABLE_CHECK_SQL, new Object[] { dbSchema(), tableName }, new int[] { Types.VARCHAR, Types.VARCHAR }, Long.class))
				.orElse(0L) != 0;
	}

	public List<String> allTables(String tablePrefix) {
		return jdbcTemplate.queryForList(ALL_TABLES_SQL, new Object[] { dbSchema(), tablePrefix }, new int[] { Types.VARCHAR, Types.VARCHAR }, String.class);
	}

	public void execute(String sql) {
		this.jdbcTemplate.execute(sql);
	}

	private String getJdbcUrl() {
		try (Connection connection = jdbcTemplate.getDataSource().getConnection()) {
			return connection.getMetaData().getURL();
		} catch (Exception e) {
			throw new IllegalStateException("get jdbc url error", e);
		}
	}

	private String dbSchema() {
		return StringUtils.substringAfterLast(StringUtils.substringBefore(jdbcUrl, "?"), "/");
	}

}
