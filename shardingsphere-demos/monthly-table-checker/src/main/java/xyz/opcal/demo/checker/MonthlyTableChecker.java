package xyz.opcal.demo.checker;

import java.nio.charset.StandardCharsets;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import xyz.opcal.demo.configuration.config.AutoDdlConfig;
import xyz.opcal.demo.configuration.config.TableConfig;
import xyz.opcal.demo.repository.jdbc.TableCheckerRepository;

@Slf4j
@Component
public class MonthlyTableChecker {

	private static final DateTimeFormatter YEAR_MONTH_FORMATTER = DateTimeFormatter.ofPattern("yyyyMM");

	private @Autowired TableCheckerRepository tableCheckerRepository;
	private @Autowired AutoDdlConfig autoDdlConfig;

	/**
	 * mock schedules job, check and create table when initial
	 */
	@PostConstruct
	public void init() {
		final YearMonth current = YearMonth.now();
		checkAndCreateTable(current.format(YEAR_MONTH_FORMATTER));
		checkAndCreateTable(current.plusMonths(1).format(YEAR_MONTH_FORMATTER));
		checkAndCreateTable(current.plusMonths(2).format(YEAR_MONTH_FORMATTER));
	}

	public void checkAndCreateTable(String month) {
		autoDdlConfig.getTables().forEach(tableConfig -> checkAndCreateTable(month, tableConfig));
	}

	public void checkAndCreateTable(String month, TableConfig tableConfig) {
		final String tableName = tableConfig.getTablePrefix() + month;
		if (!tableCheckerRepository.existeTable(tableName)) {
			final String createTableSql = createTableSql(month, tableConfig.getDdlFileName());
			log.info("creating table [{}] ddl sql [{}]", tableName, createTableSql);
			tableCheckerRepository.execute(createTableSql);
		}
	}

	public String createTableSql(String month, String fileName) {
		try {
			ExpressionParser parser = new SpelExpressionParser();
			Expression expression = parser.parseExpression(
					IOUtils.toString(ResourceUtils.getURL("classpath:templates/ddl/" + fileName), StandardCharsets.UTF_8), new TemplateParserContext());
			StandardEvaluationContext context = new StandardEvaluationContext();
			context.setVariable("month", month);
			return expression.getValue(context, String.class);
		} catch (Exception e) {
			throw new IllegalStateException("create table sql error", e);
		}
	}

}
