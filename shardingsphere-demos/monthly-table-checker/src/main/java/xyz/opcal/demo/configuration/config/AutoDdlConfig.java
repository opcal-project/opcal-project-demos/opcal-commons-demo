package xyz.opcal.demo.configuration.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties("opcal.auto.ddl")
public class AutoDdlConfig {

	private List<TableConfig> tables = new ArrayList<>();
}
