package xyz.opcal.demo.configuration.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TableConfig {

	private String tablePrefix;
	private String ddlFileName;
}
