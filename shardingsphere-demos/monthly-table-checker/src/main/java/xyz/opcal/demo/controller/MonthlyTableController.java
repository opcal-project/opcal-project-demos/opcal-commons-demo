package xyz.opcal.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.demo.checker.MonthlyTableChecker;
import xyz.opcal.demo.repository.jdbc.TableCheckerRepository;

@RestController
@RequestMapping("/monthly/tables")
public class MonthlyTableController {

	private @Autowired MonthlyTableChecker monthlyTableChecker;
	private @Autowired TableCheckerRepository tableCheckerRepository;

	@PostMapping("/create")
	public String checkAndCreate(@RequestBody String[] months) {
		for (String month : months) {
			monthlyTableChecker.checkAndCreateTable(month);
		}
		return "ok";
	}

	@GetMapping("/")
	public List<String> allTables() {
		return tableCheckerRepository.allTables("sharding_log%");
	}

}
