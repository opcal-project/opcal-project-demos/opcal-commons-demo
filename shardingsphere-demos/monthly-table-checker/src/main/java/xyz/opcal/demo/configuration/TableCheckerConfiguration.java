package xyz.opcal.demo.configuration;


import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import xyz.opcal.demo.configuration.config.AutoDdlConfig;

@Configuration
@EnableConfigurationProperties(value = { AutoDdlConfig.class })
public class TableCheckerConfiguration {

}
