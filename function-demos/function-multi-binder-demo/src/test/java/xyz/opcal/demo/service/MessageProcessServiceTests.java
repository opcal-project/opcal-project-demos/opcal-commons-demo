package xyz.opcal.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.junit.jupiter.Testcontainers;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.demo.TcConstants;
import xyz.opcal.demo.configuration.RabbitMQTcConfiguration;
import xyz.opcal.demo.kafka.KafkaContainerCluster;

@Slf4j
@Testcontainers
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Import(RabbitMQTcConfiguration.class)
class MessageProcessServiceTests {

	static final String CREATE_URL = "/message/create?batch={batch}";

	static KafkaContainerCluster kafkaContainerCluster = new KafkaContainerCluster(3, 3);

	@DynamicPropertySource
	static void dynamicProperties(DynamicPropertyRegistry registry) {
		kafkaContainerCluster.start();
		registry.add("spring.rabbitmq.virtual-host", () -> TcConstants.TC_RABBITMQ_VHOST);
		registry.add("spring.kafka.bootstrap-servers", kafkaContainerCluster::getBootstrapServers);
	}

	@Autowired
	TestRestTemplate restTemplate;

	@Autowired
	UserProcessService userProcessService;

	Random random = new Random();

	@Test
	@Order(0)
	void create() {
		Map<String, Object> urlVariables = new HashMap<>();
		var batch = random.nextInt(10, 50);
		urlVariables.put("batch", batch);
		var response = restTemplate.postForEntity(CREATE_URL, null, String.class, urlVariables);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		log.info("$$$$$$ created message size {}", batch);
	}

	@Test
	@Order(1)
	void ageCount() {
		Awaitility.await().atMost(Duration.ofSeconds(60)).until(() -> userProcessService.consumeCount() > 0);
	}

}
